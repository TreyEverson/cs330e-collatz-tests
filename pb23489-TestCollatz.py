#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "155 455\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 155)
        self.assertEqual(j, 455)

    def test_read3(self):
        s = "978 1492\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 978)
        self.assertEqual(j, 1492)

    def test_read4(self):
        s = "58 93\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 58)
        self.assertEqual(j, 93)

    # ----
    # eval
    # ----

    def test_eval1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval5(self):
        v = collatz_eval(5812, 452)
        self.assertEqual(v, 238)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 1, 9999, 262)
        self.assertEqual(w.getvalue(), "1 9999 262\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 5812, 452, 238)
        self.assertEqual(w.getvalue(), "5812 452 238\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("2000 5389\n845 3214\n70 100\n84 48\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2000 5389 238\n845 3214 217\n70 100 119\n84 48 116\n")

    def test_solve3(self):
        r = StringIO("5256 7653\n487 200\n30 79\n1 8000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5256 7653 262\n487 200 144\n30 79 116\n1 8000 262\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 15 tests in 0.045s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
...............
----------------------------------------------------------------------
Ran 15 tests in 0.045s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          35      0     14      0   100%
TestCollatz.py      68      0      0      0   100%
------------------------------------------------------------
TOTAL              103      0     14      0   100%
"""
